<?php

$app->get('/valor', function() use($app) {
  $app->response->headers->set('Content-Type', 'application/xml');
  $datos = array(
    'valor' => array(
    )
  );
  $valor = $app->db->query("select * from valor");
  foreach ($valor as $valor) 
  {
  $datos['valor'][] = $valor;
  }
  $app->render('xml.php', $datos);
});


//valor con identificador

  $app->get('/valor/:id', function($id) use($app) { 
  $app->response->headers->set('Content-Type', 'application/xml');
  $datos = array(
    'valor' => array(
    )
  );
  $valor = $app->db->query("select * from valor where id_valor =".$id);
  
  if ($valor->rowCount() != 1) {
    $app->halt(404, "Error: El libro con id '$id' no existe.");
  }

  foreach ($valor as $valor) 
  {
  $datos['valor'][] = $valor;
  }
  $app->response->setStatus(200);	
  $app->render('xml.php', $datos);
});


///Notas	
$app->get('/nota', function() use($app) {
  $app->response->headers->set('Content-Type', 'application/xml');
  $nota = array(
    'nota' => array(
    )
  );
  $query= $app->db->query("select * from valor_notas");
  foreach ($query as $query) 
  {
  $nota['nota'][] = $query;
  }
  $app->render('xml1.php', $nota);
});


//nota con identificador
 

  $app->get('/nota/:id', function($id) use($app) { 
  $app->response->headers->set('Content-Type', 'application/xml');
  $datos = array(
    'nota' => array(
    )
  );
  $valor = $app->db->query("select * from valor_notas where id_valor_notas =".$id);
  
  if ($valor->rowCount() != 1) {
    $app->halt(404, "Error: id '$id' no existe.");
  }

  foreach ($valor as $valor) 
  {
  $datos['nota'][] = $valor;
  }
  $app->response->setStatus(200);	
  $app->render('xml1.php', $datos);
});


///Fecha	
$app->get('/fecha', function() use($app) {
  $app->response->headers->set('Content-Type', 'application/xml');
  $datos = array(
    'fecha' => array(
    )
  );
  $query= $app->db->query("select * from valor_anio");
  foreach ($query as $query) 
  {
  $datos['fecha'][] = $query;
  }
  $app->render('xml2.php', $datos);
});


//fecha con identificador

  $app->get('/fecha/:id', function($id) use($app) { 
  $app->response->headers->set('Content-Type', 'application/xml');
  $datos = array(
    'fecha' => array(
    )
  );
  $valor = $app->db->query("select * from valor_anio where id_valor_anio =".$id);
  
  if ($valor->rowCount() != 1) {
    $app->halt(404, "Error: id '$id' no existe.");
  }

  foreach ($valor as $valor) 
  {
  $datos['fecha'][] = $valor;
  }
  $app->response->setStatus(200);	
  $app->render('xml2.php', $datos);
});


//fecha con identificador

  $app->delete('/fecha/:id', function($id) use($app) { 
  $app->response->headers->set('Content-Type', 'application/xml');
  $valor = $app->db->query("delete from valor_anio where id_valor_anio =".$id);
 
  if ($valor->rowCount() != 1) {
    $app->halt(404, "Error: id '$id' no existe.");
  }
  $app->response->setStatus(200);	
  $app->render('xml2.php', $datos);
});

//valor con identificador

$app->delete('/valor/:id', function($id) use($app) {


$query="delete from valor where id_valor=".$id;
$qry=$app->db->prepare($query);
if($qry->execute() === false)
{
$app->halt(500,"error del servidor");
}
if($qry->rowCount() == 0){
$app->halt(404,"error recurso no encontrado");

}

$app->halt(200,"operacion satisfactoria");

});

//nota con identificador

  $app->delete('/nota/:id', function($id) use($app) { 
  $app->response->headers->set('Content-Type', 'application/xml');

  $valor = $app->db->query("delete from valor_nota where id_valor_notas =".$id);
  
  if ($valor->rowCount() != 1) {
    $app->halt(404, "Error: id '$id' no existe.");
  }
  $app->response->setStatus(200);	
  $app->render('xml1.php', $datos);
});



function getTitleFromUrl($url)
{
    preg_match('/<title>(.+)<\/title>/', file_get_contents($url), $matches);
    return mb_convert_encoding($matches[1], 'UTF-8', 'UTF-8');
}

$app->put('/valor/:id', function ($id) use ($db, $app) {
    $sth = $db->prepare('UPDATE valor SET id_valor = ?, tipo = ? WHERE id_valor = ?;');
    $sth->execute([
        $app->request()->post('id_valor'),
        $app->request()->post('tipo'),
        intval($id),
    ]);
});


$app->post('/valor/:id/:tipo/:tema1/:tema2/:tema3/:indicador', function($id, $tipo, $tema1, $tema2, $tema3, $indicador) use($app){
	 $app->response->headers->set('Content-Type', 'application/xml');
	 $app->db->query("insert into valor values($id, $tipo, $tema1, $tema2, $tema3, $indicador);");
  $app->render('xml.php');
} );


$app->post('/fecha/:id/:diez1/:diez2/:diez3/:diez4/:once1/:once2/:once3/:once4/:doce1/:doce2/:doce3/:doce4/:trece1/:trece2/:trece3/:trece4/:catorce1/:medida', 
function($id,$diez1,$diez2,$diez3,$diez4,$once1,$once2,$once3,$once4,$doce1,$doce2,$doce3,$doce4,$trece1,$trece2,$trece3,$trece4,$catorce1,$medida) use($app) {
  $app->response->headers->set('Content-Type', 'application/xml');
  $datos = array(
    'fecha' => array());
  $app->db->query("insert into valor_anio values($id,$diez1,$diez2,$diez3,$diez4,$once1,$once2,$once3,$once4,$doce1,$doce2,$doce3,$doce4,$trece1,$trece2,$trece3,$trece4,$catorce1,$medida)");
  $app->render('xml2.php', $datos);
});
