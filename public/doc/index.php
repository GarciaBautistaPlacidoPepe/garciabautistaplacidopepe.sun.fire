<!DOCTYPE html>
  <head>
    <meta charset="utf-8" />
    <title>Documentación de API</title>
    <link  rel="stylesheet" href="../css/bootstrap.min.css" >
    <link  rel="stylesheet" href="../css/bootstrap.css"> 
  </head>
  <body>
    <a href="../">Atras</a>
    <hr />
    <h1>Documentación de API</h1>
    <p>
      La API se basa en los principios de REST con el framework slim y muestra indicadores de ocupación y empleo a nivel nacional del año 2010-2014 por periodos.
    </p>
    <p>
      La URL raiz es <a 
      href="http://garciabautistaplacidopepe.sun.fire/api/">http://garciabautistaplacidopepe.sun.fire/api/</a>.
    </p>
    <hr />

<TABLE BORDER=1 ALIGN=CENTER>

	<TR>
		<TH>
		Recurso
		</TH>

		<TH>
		Método
		</TH>

		<TH  >
		Descripción
		</TH>
		
		<TH >
		Ejemplo
		</TH>
	</TR>

	<TR>
		<TD>
<p>Valor</p>
		</TD>

		<TD>
<p> GET /valor-</p>
		</TD>

		<TD>
				<p>Devuelve una lista de valores en base a empleo y relaciones laborales a nivel nacional</p>
				<ul>
				  <li><strong>URL</strong>:http://garciabautistaplacidopepe.sun.fire/api/valor</li>
				  <li><strong>Método HTTP</strong>: GET</li>
				  <li><strong>Presentación de respuesta</strong>: XML</li>
				</ul>

		</TD>

		<TD>
			<li><strong>Ejemplo de respuesta</strong>:
				<p>
				&lt;?xml version="1.0" encoding="UTF-8"?&gt; </br>
				&lt;valor id_valor="1007000018"&gt;</br>
					&lt;tipo&gt;Nacional&lt;/tipo&gt;</br>
					&lt; temaNivel1 &gt;Sociedad y Gobierno&lt;/temaNivel1&gt;</br>
					&lt; temaNivel2&gt;Empleo y relaciones laborales&lt;/temaNivel2&gt;</br>
					&lt;temaNivel3&gt;Características del empleo de la población&lt;/temaNivel3&gt;</br>
					&lt;indicador&gt;Población de 14 y más años&lt;/indicador&gt;</br>
				&lt;/valor>
				<p>
			 </li>
		</TD>
	</TR>
    <TR>
	<TR>
		<TD>
<p>Valor</p>
		</TD>

		<TD>
<p> POST /valor/id </p>
		</TD>

		<TD>
				<p>crea un nuevo valor en base a empleo y relaciones laborales a nivel nacional</p>
				<ul>
				  <li><strong>URL</strong>:http://garciabautistaplacidopepe.sun.fire/api/valor</li>
				  <li><strong>Método HTTP</strong>: GET</li>
				  <li><strong>Presentación de respuesta</strong>: XML</li>
				</ul>

		</TD>

		<TD>
			<li><strong>Ejemplo de respuesta</strong>:
				<p>
				&lt;?xml version="1.0" encoding="UTF-8"?&gt; </br>
				&lt;valor id_valor="1007000018"&gt;</br>
					&lt;tipo&gt;Nacional&lt;/tipo&gt;</br>
					&lt; temaNivel1 &gt;Sociedad y Gobierno&lt;/temaNivel1&gt;</br>
					&lt; temaNivel2&gt;Empleo y relaciones laborales&lt;/temaNivel2&gt;</br>
					&lt;temaNivel3&gt;Características del empleo de la población&lt;/temaNivel3&gt;</br>
					&lt;indicador&gt;Población de 14 y más años&lt;/indicador&gt;</br>
				&lt;/valor>
				<p>
			 </li>
		</TD>
	</TR>
    <TR>

		<TD >
			<p>Valorid</p>
		</TD>

		<TD >
			<p> GET /valor/id-</p>
		</TD>
    <TD >
				<p>Devuelve un valor específico</p>
				<ul>
				  <li><strong>URL</strong>:http://garciabautistaplacidopepe.sun.fire/api/api/valor/id</li>
				  <li><strong>Método HTTP</strong>: GET</li>
				  <li><strong>Parámetros</strong>:
				  <ul>
					<li>id: identificador de empleos y ocupaciones</li> 
				  </ul>
				  </li>
				  <li><strong>Presentación de respuesta</strong>: XML</li>
				</ul>

		</TD>

		<TD >
			<li><strong>Ejemplo de respuesta</strong>:
				<p>
				&lt;?xml version="1.0" encoding="UTF-8"?&gt; </br>
				&lt;valor id_valor="1007000018"&gt;</br>
					&lt;tipo&gt;Nacional&lt;/tipo&gt;</br>
					&lt; temaNivel1 &gt;Sociedad y Gobierno&lt;/temaNivel1&gt;</br>
					&lt; temaNivel2&gt;Empleo y relaciones laborales&lt;/temaNivel2&gt;</br>
					&lt;temaNivel3&gt;Características del empleo de la población&lt;/temaNivel3&gt;</br>
					&lt;indicador&gt;Población de 14 y más años&lt;/indicador&gt;</br>
				&lt;/valor>
				<p>
			 </li>
		</TD>
	</TR>
	<TR>
		<TD>
<p>fecha</p>
		</TD>

		<TD>
<p> GET /fecha-</p>
		</TD>

		<TD>
				<p>Devuelve una lista de indicadores cuantitatívos distribuido en fechas</p>
				<ul>
				  <li><strong>URL</strong>:http://garciabautistaplacidopepe.sun.fire/api/fecha</li>
				  <li><strong>Método HTTP</strong>: GET</li>
				  <li><strong>Presentación de respuesta</strong>: XML</li>
				</ul>

		</TD>

		<TD>
			<li><strong>Ejemplo de respuesta</strong>:
				<p>
				&lt;?xml version="1.0" encoding="UTF-8"?&gt; </br>
 &lt; a2010-1 &gt; 83400615 &lt; /a2010-1 &gt; </br>
 &lt; a2010-2 &gt; 83769901 &lt; /a2010-2 &gt; </br>
 &lt; a2010-3 &gt; 84062608 &lt; /a2010-3 &gt; </br>
 &lt; a2010-4 &gt; 84452737 &lt; /a2010-4 &gt; </br>
 &lt; a2011-1 &gt; 84785402 &lt; /a2011-1 &gt; </br>
 &lt; a2011-2 &gt; 84785402 &lt; /a2011-2 &gt; </br>
 &lt; a2011-3 &gt; 84785402 &lt; /a2011-3 &gt; </br>
 &lt; a2011-4 &gt; 84785402 &lt; /a2011-4 &gt; </br>
 &lt; a2012-1 &gt; 86376976 &lt; /a2012-1 &gt; </br>
 &lt; a2012-2 &gt; 86376976 &lt; /a2012-2 &gt; </br>
 &lt; a2012-3 &gt; 86376976 &lt; /a2012-3 &gt; </br>
 &lt; a2012-4 &gt; 86376976 &lt; /a2012-4 &gt; </br>
 &lt; a2013-1 &gt; 87836305 &lt; /a2013-1 &gt; </br>
 &lt; a2013-2 &gt; 87836305 &lt; /a2013-2 &gt; </br>
 &lt; a2013-3 &gt; 87836305 &lt; /a2013-3 &gt; </br>
 &lt; a2013-4 &gt; 87836305 &lt; /a2013-4 &gt; </br>
 &lt; a2014-1 &gt; 88595829 &lt; /a2014-1 &gt; 
 &lt; unidadMedida &gt; Número de personas &lt; unidadMedida &gt; </br>
				<p>
			 </li>
		</TD>
	</TR>
    <TR>
		<TD >
			<p>fechaid</p>
		</TD>

		<TD >
			<p> GET /fecha/id-</p>
		</TD>
    <TD >
				<p>Devuelve un valor específico en base a un identificador por periodos</p>
				<ul>
				  <li><strong>URL</strong>:http://garciabautistaplacidopepe.sun.fire/api/fecha/id</li>
				  <li><strong>Método HTTP</strong>: GET</li>
				  <li><strong>Parámetros</strong>:
				  <ul>
					<li>id: identificador de empleos y ocupaciones</li> 
				  </ul>
				  </li>
				  <li><strong>Presentación de respuesta</strong>: XML</li>
				</ul>

		</TD>

		<TD >
			<li><strong>Ejemplo de respuesta</strong>:
				<p>
				&lt;?xml version="1.0" encoding="UTF-8"?&gt;
				&lt;?xml version="1.0" encoding="UTF-8"?&gt; </br>
 &lt; a2010-1 &gt; 83400615 &lt; /a2010-1 &gt; </br>
 &lt; a2010-2 &gt; 83769901 &lt; /a2010-2 &gt; </br>
 &lt; a2010-3 &gt; 84062608 &lt; /a2010-3 &gt; </br>
 &lt; a2010-4 &gt; 84452737 &lt; /a2010-4 &gt; </br>
 &lt; a2011-1 &gt; 84785402 &lt; /a2011-1 &gt; </br>
 &lt; a2011-2 &gt; 84785402 &lt; /a2011-2 &gt; </br>
 &lt; a2011-3 &gt; 84785402 &lt; /a2011-3 &gt; </br>
 &lt; a2011-4 &gt; 84785402 &lt; /a2011-4 &gt; </br>
 &lt; a2012-1 &gt; 86376976 &lt; /a2012-1 &gt; </br>
 &lt; a2012-2 &gt; 86376976 &lt; /a2012-2 &gt; </br>
 &lt; a2012-3 &gt; 86376976 &lt; /a2012-3 &gt; </br>
 &lt; a2012-4 &gt; 86376976 &lt; /a2012-4 &gt; </br>
 &lt; a2013-1 &gt; 87836305 &lt; /a2013-1 &gt; </br>
 &lt; a2013-2 &gt; 87836305 &lt; /a2013-2 &gt; </br>
 &lt; a2013-3 &gt; 87836305 &lt; /a2013-3 &gt; </br>
 &lt; a2013-4 &gt; 87836305 &lt; /a2013-4 &gt; </br>
 &lt; a2014-1 &gt; 88595829 &lt; /a2014-1 &gt; 
 &lt; unidadMedida &gt; Número de personas &lt; unidadMedida &gt; </br>
				<p>
			 </li>
		</TD>
	</TR>

  <TR>
		<TD >
			<p>fechaid</p>
		</TD>

		<TD >
			<p> POS /fecha/id-</p>
		</TD>
    <TD >
				<p>ingresa un valor específico en base a un identificador a la entidad fecha</p>
				<ul>
				  <li><strong>URL</strong>:http://garciabautistaplacidopepe.sun.fire/api/fecha/id</li>
				  <li><strong>Método HTTP</strong>: GET</li>
				  <li><strong>Parámetros</strong>:
				  <ul>
					<li>id: identificador de fecha en empleos y ocupaciones</li> 
				  </ul>
				  </li>
				  <li><strong>Presentación de respuesta</strong>: XML</li>
				</ul>

		</TD>

		<TD >
			<li><strong>Ejemplo de respuesta</strong>:
				<p>
				&lt;?xml version="1.0" encoding="UTF-8"?&gt;
				&lt;?xml version="1.0" encoding="UTF-8"?&gt; </br>
 &lt; a2010-1 &gt; 83400615 &lt; /a2010-1 &gt; </br>
 &lt; a2010-2 &gt; 83769901 &lt; /a2010-2 &gt; </br>
 &lt; a2010-3 &gt; 84062608 &lt; /a2010-3 &gt; </br>
 &lt; a2010-4 &gt; 84452737 &lt; /a2010-4 &gt; </br>
 &lt; a2011-1 &gt; 84785402 &lt; /a2011-1 &gt; </br>
 &lt; a2011-2 &gt; 84785402 &lt; /a2011-2 &gt; </br>
 &lt; a2011-3 &gt; 84785402 &lt; /a2011-3 &gt; </br>
 &lt; a2011-4 &gt; 84785402 &lt; /a2011-4 &gt; </br>
 &lt; a2012-1 &gt; 86376976 &lt; /a2012-1 &gt; </br>
 &lt; a2012-2 &gt; 86376976 &lt; /a2012-2 &gt; </br>
 &lt; a2012-3 &gt; 86376976 &lt; /a2012-3 &gt; </br>
 &lt; a2012-4 &gt; 86376976 &lt; /a2012-4 &gt; </br>
 &lt; a2013-1 &gt; 87836305 &lt; /a2013-1 &gt; </br>
 &lt; a2013-2 &gt; 87836305 &lt; /a2013-2 &gt; </br>
 &lt; a2013-3 &gt; 87836305 &lt; /a2013-3 &gt; </br>
 &lt; a2013-4 &gt; 87836305 &lt; /a2013-4 &gt; </br>
 &lt; a2014-1 &gt; 88595829 &lt; /a2014-1 &gt; 
 &lt; unidadMedida &gt; Número de personas &lt; unidadMedida &gt; </br>
				<p>
			 </li>
		</TD>
	</TR>



	<TR>
		<TD>
<p>nota</p>
		</TD>

		<TD>
<p> GET /nota-</p>
		</TD>

		<TD>
				<p>Devuelve una lista de notas en base a empleo y relaciones laborales a nivel nacional</p>
				<ul>
				  <li><strong>URL</strong>:http://garciabautistaplacidopepe.sun.fire/api/nota</li>
				  <li><strong>Método HTTP</strong>: GET</li>
				  <li><strong>Presentación de respuesta</strong>: XML</li>
				</ul>

		</TD>

		<TD>
			<li><strong>Ejemplo de respuesta</strong>:
				<p>
				&lt;?xml version="1.0" encoding="UTF-8"?&gt; </br>
&lt; nota id_valor_notas="1007000018" &gt; </br>
&lt; nota &gt; Datos ajustados a los montos poblacionales de las proyecciones demográficas del CONAPO. &lt;/nota &gt; </br>
&lt; /nota &gt;
				<p>
			 </li>
		</TD>
	</TR>

    <TR>
		<TD >
			<p>notaid</p>
		</TD>

		<TD >
			<p> GET /nota/id-</p>
		</TD>
    <TD >
				<p>Devuelve un valor específico de una nota</p>
				<ul>
				  <li><strong>URL</strong>:http://garciabautistaplacidopepe.sun.fire/api/nota/id</li>
				  <li><strong>Método HTTP</strong>: GET</li>
				  <li><strong>Parámetros</strong>:
				  <ul>
					<li>id: identificador de empleos y ocupaciones</li> 
				  </ul>
				  </li>
				  <li><strong>Presentación de respuesta</strong>: XML</li>
				</ul>

		</TD>

		<TD >
			<li><strong>Ejemplo de respuesta</strong>:
				<p>
				&lt;?xml version="1.0" encoding="UTF-8"?&gt;
				&lt;?xml version="1.0" encoding="UTF-8"?&gt; </br>
&lt; nota id_valor_notas="1007000018" &gt; </br>
&lt; nota &gt; Datos ajustados a los montos poblacionales de las proyecciones demográficas del CONAPO. &lt;/nota &gt; </br>
&lt; /nota &gt;
				<p>
			 </li>
		</TD>
	</TR>

<TR>
		<TD >
			<p>notaid</p>
		</TD>

		<TD >
			<p> POST /nota/id-</p>
		</TD>
    <TD >
				<p>ingresa valores a una nota</p>
				<ul>
				  <li><strong>URL</strong>:http://garciabautistaplacidopepe.sun.fire/api/nota/id</li>
				  <li><strong>Método HTTP</strong>: GET</li>
				  <li><strong>Parámetros</strong>:
				  <ul>
					<li>id: identificador de empleos y ocupaciones</li> 
				  </ul>
				  </li>
				  <li><strong>Presentación de respuesta</strong>: XML</li>
				</ul>

		</TD>

		<TD >
			<li><strong>Ejemplo de respuesta</strong>:
				<p>
				&lt;?xml version="1.0" encoding="UTF-8"?&gt;
				&lt;?xml version="1.0" encoding="UTF-8"?&gt; </br>
&lt; nota id_valor_notas="1007000018" &gt; </br>
&lt; nota &gt; Datos ajustados a los montos poblacionales de las proyecciones demográficas del CONAPO. &lt;/nota &gt; </br>
&lt; /nota &gt;
				<p>
			 </li>
		</TD>
</TR>
<TR>
   <TD>
   <p>notas</p>
    </TD >			
   <TD>
   <p> PUT /nota/id-</p>
    </TD >
                            <TD>

				<p>Actualiza un valor de empleos y relaciones</p>
				<ul>
				  <li><strong>URL</strong>:http://garciabautistaplacidopepe.sun.fire/api/nota/id</li>
				  <li><strong>Método HTTP</strong>: GET</li>
				  <li><strong>Parámetros</strong>:
				  <ul>
					<li>id: identificador de empleos y ocupaciones</li> 
				  </ul>
				  </li>
				  <li><strong>Presentación de respuesta</strong>: XML</li>
				</ul>

		            </TD>

		<TD >
			<li><strong>Ejemplo de respuesta</strong>:
				<p>
				&lt;?xml version="1.0" encoding="UTF-8"?&gt;
				&lt;?xml version="1.0" encoding="UTF-8"?&gt; </br>
&lt; nota id_valor_notas="1007000018" &gt; </br>
&lt; nota &gt; Datos ajustados a los montos poblacionales de las proyecciones demográficas del CONAPO. &lt;/nota &gt; </br>
&lt; /nota &gt;
				<p>
			 </li>
		</TD>
	</TR>
<TR>
    <TD>
<p> eliminanota</p>
    </TD >			
    <TD>
<p> DELETE /nota/id-</p>
    </TD >
<TD>

				<p>Elimina el valor y relaciones en base a un identificador</p>
				<ul>
				  <li><strong>URL</strong>:http://garciabautistaplacidopepe.sun.fire/api/nota/id</li>
				  <li><strong>Método HTTP</strong>: GET</li>
				  <li><strong>Parámetros</strong>:
				  <ul>
					<li>id: identificador de empleos y ocupaciones</li> 
				  </ul>
				  </li>
				  <li><strong>Presentación de respuesta</strong>: XML</li>
				</ul>

		</TD>

		<TD >
			<li><strong>Ejemplo de respuesta</strong>:
				<p>
				&lt;?xml version="1.0" encoding="UTF-8"?&gt;
				&lt;?xml version="1.0" encoding="UTF-8"?&gt; </br>
&lt; nota id_valor_notas="1007000018" &gt; </br>
&lt; nota &gt; Datos ajustados a los montos poblacionales de las proyecciones demográficas del CONAPO. &lt;/nota &gt; </br>
&lt; /nota &gt;
				<p>
			 </li>
		</TD>
	</TR>

</TABLE>
</body>
</html>
